import { CardData, CardType, LayoutData } from './models';

export const mockCardData: CardData[] = [
  {
    id: 1,
    data: {
      type: CardType.Text,
      text: 'This is the content text',
    },
  },
  {
    id: 2,
    data: {
      type: CardType.Text,
      text: 'And this is another text',
    },
  },
  {
    id: 3,
    data: {
      type: CardType.Table,
      tableHeader: 'This is the table header',
    },
  },
  {
    id: 4,
    data: {
      type: CardType.Chart,
      chartDescription: 'This is the chart description',
    },
  },
];

export const mockLayoutDataList: LayoutData[] = [
  { id: 1, position: [0, 0, 3, 3] },
  { id: 2, position: [0, 0, 3, 3] },
  { id: 3, position: [3, 0, 6, 3] },
  { id: 4, position: [0, 0, 3, 3] },
];
