import { Component, Input, OnInit } from '@angular/core';
import { TableData } from '../../models';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() data: TableData;

  constructor() {}

  ngOnInit(): void {}
}
