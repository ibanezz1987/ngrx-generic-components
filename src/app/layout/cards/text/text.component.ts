import { Component, Input, OnInit } from '@angular/core';
import { TextData } from '../../models';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
})
export class TextComponent implements OnInit {
  @Input() data: TextData;

  constructor() {}

  ngOnInit(): void {}
}
