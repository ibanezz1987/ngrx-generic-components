import { Component, Input, OnInit } from '@angular/core';
import { ChartData } from '../../models';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
})
export class ChartComponent implements OnInit {
  @Input() data: ChartData;

  constructor() {}

  ngOnInit(): void {}
}
