import { EntityState } from '@ngrx/entity';

// Card data

export enum CardType {
  Text = 'Text',
  Chart = 'Chart',
  Table = 'Table',
}

export interface TextData {
  type: CardType.Text;
  text: string;
}

export interface TableData {
  type: CardType.Table;
  tableHeader: string;
}

export interface ChartData {
  type: CardType.Chart;
  chartDescription: string;
}

export interface CardData {
  id: number;
  data: TextData | TableData | ChartData;
}

export interface CardDataState {
  cardData?: CardData[];
}

export interface CardDataState extends EntityState<CardDataStateEntity> {}

export interface CardDataStateEntity extends CardData {
  id: number;
}

// Layout data

export interface LayoutData {
  id: number;
  position: [number, number, number, number];
}

export interface LayoutDataState {
  layoutData?: LayoutData[];
}

export interface LayoutDataState extends EntityState<LayoutDataStateEntity> {}

export interface LayoutDataStateEntity extends LayoutData {
  id: number;
}

// Combined data

export interface CardDataAndPosition extends CardData {
  layoutData: LayoutData;
}
