import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { CardData, CardDataState } from '../models';
import { take } from 'rxjs/operators';
import * as LayoutSelectors from '../store/card-data-store/selectors';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
})
export class DetailComponent implements OnInit {
  cardId: number;
  card: CardData;

  constructor(
    private readonly store: Store<CardDataState>,
    private readonly route: ActivatedRoute,
    protected readonly router: Router
  ) {}

  ngOnInit(): void {
    this.cardId = this.route.snapshot.params['id'];

    this.store
      .pipe(select(LayoutSelectors.selectCardData(this.cardId)), take(1))
      .subscribe((card) => (this.card = card));
  }

  goBack() {
    this.router.navigate(['layout']);
  }
}
