import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LayoutComponent } from './layout.component';
import { ComponentRoutingModule } from './layout-routing.module';
import { cardDataReducer } from './store/card-data-store/reducer';
import { CardDataEffects } from './store/card-data-store/effects';
import { TextComponent } from './cards/text/text.component';
import { TableComponent } from './cards/table/table.component';
import { ChartComponent } from './cards/chart/chart.component';
import { CommonModule } from '@angular/common';
import { DetailComponent } from './detail/detail.component';
import { layoutDataReducer } from './store/layout-data-store/reducer';

@NgModule({
  imports: [
    CommonModule,
    ComponentRoutingModule,
    StoreModule.forFeature('layoutData', layoutDataReducer),
    StoreModule.forFeature('cardData', cardDataReducer),
    EffectsModule.forFeature([CardDataEffects]),
  ],
  declarations: [
    LayoutComponent,
    TextComponent,
    TableComponent,
    ChartComponent,
    DetailComponent,
  ],
})
export class LayoutModule {}
