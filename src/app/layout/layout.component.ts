import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { mockCardData, mockLayoutDataList } from './mockdata';
import { CardDataAndPosition, CardDataState } from './models';
import * as CardDataActions from './store/card-data-store/actions';
import * as LayoutDataActions from './store/layout-data-store/actions';
import * as CardDataSelectors from './store/card-data-store/selectors';
import * as LayoutDataSelectors from './store/layout-data-store/selectors';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  cardDataList$ = this.store.pipe(select(CardDataSelectors.selectCardDataList));
  layoutDataList$ = this.store.pipe(
    select(LayoutDataSelectors.selectLayoutDataList)
  );

  constructor(
    private readonly store: Store<CardDataState>,
    protected readonly router: Router
  ) {}

  ngOnInit() {
    this.cardDataList$.pipe(take(1)).subscribe((cardDataList) => {
      if (!cardDataList.length) this.setSomeValues();

      const cardDataAndPosition = this.getCardDataAndPositionData();
      console.log('cardDataAndPosition', cardDataAndPosition);
    });

    this.layoutDataList$.pipe(take(1)).subscribe((layoutDataList) => {
      if (!layoutDataList.length) this.setSomeOtherValues();
    });
  }

  setSomeValues() {
    const cardDataList = mockCardData;

    this.store.dispatch(CardDataActions.SetCardDataList({ cardDataList }));
  }

  setSomeOtherValues() {
    const layoutDataList = mockLayoutDataList;

    this.store.dispatch(
      LayoutDataActions.SetLayoutDataList({ layoutDataList })
    );
  }

  addSomeCard() {
    this.store.dispatch(
      CardDataActions.AddCardData({
        cardData: mockCardData[Math.round(Math.random() * 4 - 0.5)],
      })
    );
  }

  getCardDataAndPositionData() {
    const cardDataList = mockCardData;
    const cardLayoutDataList = mockLayoutDataList;
    const cardDataAndPosition: CardDataAndPosition[] = [];
    cardDataList.forEach((cardData) => {
      cardDataAndPosition.push({
        ...cardData,
        layoutData: cardLayoutDataList.find(
          (mockCardPosition) => mockCardPosition.id === cardData.id
        ),
      });
    });
    return cardDataAndPosition;
  }

  removeLastCard() {
    this.cardDataList$.pipe(take(1)).subscribe((cardDataList) => {
      this.store.dispatch(
        CardDataActions.RemoveCardData({
          id: cardDataList[cardDataList.length - 1].id,
        })
      );
    });
  }

  viewDetailPage(id: number) {
    this.router.navigate(['layout', 'detail', id]);
  }
}
