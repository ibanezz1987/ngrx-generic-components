import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { LayoutComponent } from './layout.component';

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild([
      {
        path: 'layout',
        component: LayoutComponent,
      },
      {
        path: 'layout/detail/:id',
        component: DetailComponent,
      },
    ]),
  ],
})
export class ComponentRoutingModule {}
