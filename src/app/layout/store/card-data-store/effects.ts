import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import * as LayoutActions from './actions';

@Injectable()
export class CardDataEffects {
  cardsLoaded$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LayoutActions.CardsDataListLoaded),
      map(({ cardDataList }) => LayoutActions.SetCardDataList({ cardDataList }))
    )
  );

  constructor(protected actions$: Actions) {}
}
