import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { CardDataState, CardDataStateEntity } from '../../models';
import * as CardDataActions from './actions';

export const adapter: EntityAdapter<CardDataStateEntity> =
  createEntityAdapter<CardDataStateEntity>();
export const initialState: CardDataState = adapter.getInitialState({});

export const cardDataReducer = createReducer<CardDataState>(
  initialState,
  on(CardDataActions.SetCardDataList, (state, { cardDataList }) => {
    const cardsWithIds = cardDataList.map((cardData) => ({
      ...cardData,
      id: cardData.id,
    }));
    return adapter.setAll(cardsWithIds, state);
  }),
  on(CardDataActions.AddCardData, (state, { cardData }) => {
    const newCard = {
      ...cardData,
      id: !state.ids?.length ? 1 : Number(state.ids[state.ids.length - 1]) + 1,
    };
    return adapter.addOne(newCard, state);
  }),
  on(CardDataActions.RemoveCardData, (state, { id }) =>
    adapter.removeOne(id, state)
  )
);
