import { createAction, props } from '@ngrx/store';
import { CardData } from '../../models';

export const CardsDataListLoaded = createAction(
  '[Layout] Cards data list loaded',
  props<{ cardDataList: CardData[] }>()
);

export const SetCardDataList = createAction(
  '[Layout] Set card data list',
  props<{ cardDataList: CardData[] }>()
);

export const AddCardData = createAction(
  '[Layout] Add card data',
  props<{ cardData: CardData }>()
);

export const RemoveCardData = createAction(
  '[Layout] Remove card data',
  props<{ id: number }>()
);
