import { createSelector, createFeatureSelector } from '@ngrx/store';
import { CardData, CardDataState } from '../../models';
import { adapter } from './reducer';

const { selectIds, selectEntities, selectAll, selectTotal } =
  adapter.getSelectors();

export const selectCardDataState =
  createFeatureSelector<CardDataState>('cardData');

export const selectCardEntities = createSelector(
  selectCardDataState,
  selectEntities
);

export const selectCardDataList = createSelector(
  selectCardDataState,
  selectAll
);

export const selectCardData = (id: number) =>
  createSelector(selectCardEntities, (cardDataList) => cardDataList[id]);
