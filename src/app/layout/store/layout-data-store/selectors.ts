import { createSelector, createFeatureSelector } from '@ngrx/store';
import { LayoutDataState } from '../../models';
import { adapter } from './reducer';

const { selectIds, selectEntities, selectAll, selectTotal } =
  adapter.getSelectors();

export const selectLayoutDataState =
  createFeatureSelector<LayoutDataState>('layoutData');

export const selectLayoutEntities = createSelector(
  selectLayoutDataState,
  selectEntities
);

export const selectLayoutDataList = createSelector(
  selectLayoutDataState,
  selectAll
);

export const selectLayoutData = (id: number) =>
  createSelector(selectLayoutEntities, (layoutDataList) => layoutDataList[id]);
