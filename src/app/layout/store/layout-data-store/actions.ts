import { createAction, props } from '@ngrx/store';
import { LayoutData } from '../../models';

export const SetLayoutDataList = createAction(
  '[Layout] Set layout data list',
  props<{ layoutDataList: LayoutData[] }>()
);
