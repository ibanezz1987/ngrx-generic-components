import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { LayoutDataState, LayoutDataStateEntity } from '../../models';
import * as LayoutDataActions from './actions';

export const adapter: EntityAdapter<LayoutDataStateEntity> =
  createEntityAdapter<LayoutDataStateEntity>();
export const initialState: LayoutDataState = adapter.getInitialState({});

export const layoutDataReducer = createReducer<LayoutDataState>(
  initialState,
  on(LayoutDataActions.SetLayoutDataList, (state, { layoutDataList }) => {
    const layoutDataWithIds = layoutDataList.map((layoutData) => ({
      ...layoutData,
      id: layoutData.id,
    }));
    return adapter.setAll(layoutDataWithIds, state);
  })
);
